#include "stdafx.h"
#include "physics.h"
//#include"Object.h"

physics::physics()
{
}


physics::~physics()
{
}

bool physics::isCollide(Object * A, Object * B, int type)
{
	switch (type)
	{
	case 0:
		return BBColloision_test(A, B);
		break;
	case 1:
		break;
	default:
		return false;
		break;
	}

	 return false;
}

void physics::processCollision(Object * a, Object * b)
{
	//obj a 
	float vx, vy, vz;
	float mass;
	a->getVel(&vx, &vy, &vz);
	a->getMass(&mass);

	//obj b
	float vx1, vy1, vz1;
	float mass1;
	b->getVel(&vx1, &vy1, &vz1);
	b->getMass(&mass1);

	//오브젝트a의 충돌이후의 속도 계산하기
	float vfx, vfy, vfz;
	vfx = ((mass - mass1) / (mass + mass1))*vx
		+ ((2 * mass1) / (mass + mass1))*vx1;

	vfy = ((mass - mass1) / (mass + mass1))*vy
		+ ((2 * mass1) / (mass + mass1))*vy1;

	vfz = ((mass - mass1) / (mass + mass1))*vz
		+ ((2 * mass1) / (mass + mass1))*vz1;

	//calc b 
	float vfx1, vfy1, vfz1;
	vfx1 = ((2 * mass) / (mass + mass1)) * vx + ((mass1 - mass) / (mass + mass1))*vx1;
	vfy1 = ((2 * mass) / (mass + mass1)) * vy + ((mass1 - mass) / (mass + mass1))*vy1;
	vfz1 = ((2 * mass) / (mass + mass1)) * vz + ((mass1 - mass) / (mass + mass1))*vz1;

	a->setVel(vfx, vfy, vfz);
	b->setVel(vfx1, vfy1, vfz1);


}

//bool physics::ISOverlap(Object *A, Object *B, int method = 0) {
//
//
//}

bool physics::BBOverlapTest(Object *A, Object *B) {
	//A obj
	float aX, aY, aZ;
	float aSX, aSY, aSZ;
	float aMinX, aMinY, aMinZ;
	float aMaxX, aMaxY, aMaxZ;
	A->getPos(&aX, &aY, &aZ);
	A->getVol(&aSX, &aSY, &aSZ);

	aMinX = aX - aSX / 2.f; aMaxX = aX + aSX / 2.f;
	aMinY = aY - aSX / 2.f; aMaxY = aY + aSY / 2.f;
	aMinZ = aZ - aSX / 2.f; aMaxZ = aZ + aSZ / 2.f;

	//B obj
	float bX, bY, bZ;
	float bSX, bSY, bSZ;
	float bMinX, bMinY, bMinZ;
	float bMaxX, bMaxY, bMaxZ;
	B->getPos(&bX, &bY, &bZ);
	B->getVol(&bSX, &bSY, &bSZ);

	bMinX = bX - bSX / 2.f; bMaxX = bX + bSX / 2.f;
	bMinY = bY - bSX / 2.f; bMaxY = bY + bSY / 2.f;
	bMinZ = bZ - bSX / 2.f; bMaxZ = bZ + bSZ / 2.f;
	
	if (aMinX > bMaxX)
		return false;
	if (aMaxX < bMinX)
		return false;
	if (aMinY > bMaxY)
		return false;
	if (aMaxY < bMinY)
		return false;
	if (aMinZ > bMaxZ)
		return false;
	if (aMaxZ < bMinZ)
		return false;

	return true;
}

bool physics::BBColloision_test(Object *A, Object *B){
	//A obj
	float aMass, aVX, aVY, aVZ;
	A->getMass(&aMass);
	A->getVel(&aVX, &aVY, &aVZ);
	float afvx, afvy, afvz;

	//B obj
	float bMass, bVX, bVY, bVZ;
	B->getMass(&bMass);
	B->getVel(&bVX, &bVY, &bVZ);
	float bfvx, bfvy, bfvz;

	afvx = ((aMass - bMass) / (aMass + bMass))*aVX + ((bMass*2.f) / (aMass + bMass))*bVX;
	afvy = ((aMass - bMass) / (aMass + bMass))*aVY + ((bMass*2.f) / (aMass + bMass))*bVY;
	afvz = ((aMass - bMass) / (aMass + bMass))*aVZ + ((bMass*2.f) / (aMass + bMass))*bVZ;

	bfvx = (((2 * bMass) / (aMass + bMass))*aVX + ((bMass - aMass) / aMass + bMass))*bVX;
	bfvy = (((2 * bMass) / (aMass + bMass))*aVY + ((bMass - aMass) / aMass + bMass))*bVY;
	bfvz = (((2 * bMass) / (aMass + bMass))*aVZ + ((bMass - aMass) / aMass + bMass))*bVZ;

	A->setVel(afvx, afvy, afvz);
	B->setVel(bfvx, bfvy, bfvz);

	return true;
}