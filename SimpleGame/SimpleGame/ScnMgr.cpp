#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include "Object.h"
#include"Sound.h"
int g_testTex = -1;
int g_testBGTex = -1;
int m_BGM = -1;
int g_heart = -1;
int g_bullet = -1;

ScnMgr::ScnMgr()
{
	// Initialize Renderer

	m_renderer = new Renderer(500, 700);
	m_physics = new physics();
	m_Sound = new Sound();

	if (!m_renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	for (int i = 0; i < MAX_OBJ_COUNT; i++)
	{
		m_obj[i] = NULL;
	}

	m_obj[HERO_ID] = new Object();
	m_obj[HERO_ID]->setPos(0,-3, 0);
	m_obj[HERO_ID]->setVol(0.3, 0.3, 0.5);
	m_obj[HERO_ID]->setVel(0, 0, 0);
	m_obj[HERO_ID]->setColor(0, 0, 0, 0);
	m_obj[HERO_ID]->setMass(10);
	m_obj[HERO_ID]->SetFricCoef(0);
	m_obj[HERO_ID]->SetType(0);
	m_obj[HERO_ID]->SetHP(100);
	m_obj[HERO_ID]->setTexture(g_testTex);

	//int temp = AddObject(
	//	1, 1, 0,	//pos
	//	0.5, 0.5, 0.5,
	//	1, 1, 1, 1,	//rgba
	//	0, 0, 0,
	//	10,
	//	0.6,
	//	TYPE_NOMAL,
	//	100
	//);
	//시작하자마자 오른쪽으로 사라지는 흰색사각형

		//생성 시 중력 때문에 충돌이 부드럽게 되지 않을 수 있음, 캐릭터와 총알이 달라붙어서 덜덜거리면 일단은 얼추 완성

	g_testBGTex = m_renderer->GenPngTexture("./Texture/back.png");
	g_testTex = m_renderer->GenPngTexture("./Texture/user.png");
	g_heart = m_renderer->GenPngTexture("./Texture/hp.png");
	g_bullet = m_renderer->GenPngTexture("./Texture/bullet.png");

	m_BGM = m_Sound->CreateBGSound("./Sounds/bgm.mp3");//브금
	m_Sound->PlayBGSound(m_BGM, true, 0.5f);


	//g_FIRE = m_Sound->CreateShortSound("./Sounds/fire.mp3");//케릭터 애니메이션 소리
	//g_EXPL = m_Sound->CreateShortSound("./Sounds/exposion.mp3");//충돌,사망할때 출력될 보이스

}

ScnMgr::~ScnMgr(){

	float x, y, z;
	m_obj[HERO_ID]->getPos(&x, &y, &z);
	m_renderer->SetCameraPos(x*100, y*100);//카메라가 캐릭터를 따라다님
}

void ScnMgr::RenderScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
	//draw background first//배경 화면, 제일 뒤에 그려짐
	m_renderer->DrawGround(
		0.f, 0.f, 0.f,//월드 상 가운데 위치
		500.f, 700.f, 0.f,
		1, 1, 1, 1,
		g_testBGTex,
		1.f
	);

	{//여러번 그리지 말고 별도의 하트1,하트2...로 생성해서 넣고 피격당하면 화면에서 지우기
	m_renderer->DrawTextureRectAnim(
		-230, 320, 0,
		20, 20, 0.f,
		1, 1, 1, 1,
		g_heart,
		1, 1,
		1, 1);
	m_renderer->DrawTextureRectAnim(
		-200, 320, 0,
		20, 20, 0.f,
		1, 1, 1, 1,
		g_heart,
		1, 1,
		1, 1);
	m_renderer->DrawTextureRectAnim(
		-170, 320, 0,
		20, 20, 0.f,
		1, 1, 1, 1,
		g_heart,
		1, 1,
		1, 1);
	m_renderer->DrawTextureRectAnim(
		-140, 320, 0,
		20, 20, 0.f,
		1, 1, 1, 1,
		g_heart,
		1, 1,
		1, 1);
	m_renderer->DrawTextureRectAnim(
		-110, 320, 0,
		20, 20, 0.f,
		1, 1, 1, 1,
		g_heart,
		1, 1,
		1, 1);
}
	

	static float pTime = 0.f;
	pTime += 0.016f;

	//m_renderer->DrawParticle(g_particle,
	//	0, 0, 0,
	//	1,
	//	1, 1, 1, 1,
	//	0, 0,
	//	g_particle,
	//	1, pTime);
	//g_particle;

		//Draw all m_obj
	for(int i = 0; i < MAX_OBJ_COUNT; i++){
		if (m_obj[i] != NULL){
			float x, y, z;
			float sx, sy, sz;
			float vx, vy, vz;
			float r, g, b, a;
			float FricCoef;
			float mass;
			float type;
			float hp;
			int texID;

			m_obj[i]->getPos(&x, &y, &z);
			x = 100.0f * x; //convert to pixel size
			y = 100.0f * y;
			z = 100.0f * z;
			m_obj[i]->getVol(&sx, &sy, &sz);
			sx = 100.f * sx; //convert to pixel size
			sy = 100.f * sy;
			sz = 100.f * sz;
			m_obj[i]->GetColor(&r, &g, &b, &a);
			m_obj[i]->getTexture(&texID);
			
			m_renderer->DrawSolidRect(x, y, z, sx, r, g, b, a); //여기서 그리는 단위는 픽셀이였으니까
			//픽셀로 변환을 해주어야 한다. 
			

			static int temp = 0;
			int ix = (temp / 10) % 3;
			int iy = (int)(temp / 3);
			if (i == HERO_ID) {
				m_renderer->DrawTextureRectAnim(
					x, y, z,
					sx, sy, sz,
					1, 1, 1, 1,
					g_testTex,
					1, 1,
					1, 1);
			}
			else if (texID == g_bullet) {
				m_renderer->DrawTextureRectAnim
				(x, y, z,
					sx, sy, sz,
					1, 1, 1, 1,
					g_bullet,
					1, 1,
					1, 1);
			}
			temp++;
		}
	}

	for (int src = 0; src < MAX_OBJ_COUNT; src++) {
		for (int dst = 0; dst < MAX_OBJ_COUNT; dst++) {
			if (m_obj[src] != NULL && m_obj[dst] != NULL) {//널값이 아닐때만 반응하도록
				//if (m_physics->ISOverlap(m_obj[src], m_obj[dst])) {
				//	std::cout << "Collision : (" << src << " , " << dst << ")" << std::endl;
				//}
			}
		}
	}
	


}

void ScnMgr::Update(float eTiemInSecond){
	//echo current key input state

	//charactor control : Hero / 히어로라는 캐릭터가 움직이는지 확인

	std::cout << "w : " << m_keyU
			  << "s : " << m_keyD
		   	  << "a : " << m_keyL
			  << "d : " << m_keyR << std::endl;

	//힘
	float fx = 0.0f;
	float fy = 0.0f;
	float fz = 0.0f;
	//방향키가 눌렸을때마다 어느정도의 힘이 적용될것인가 
	float fAmount = 100.f;

	//if (m_keyU) fy += fAmount;
	//if (m_keyD) fy -= fAmount;
	//if (m_keyL) fx -= fAmount;
	//if (m_keyR) fx += fAmount;

	if (m_keyU) fy += fAmount;
	if (m_keyD) fy -= fAmount;
	if (m_keyL) fx -= fAmount;
	if (m_keyR) fx += fAmount;



	m_obj[HERO_ID]->AddForce(fx, fy, fz, eTiemInSecond);

	float fsize = sqrtf(fx * fx + fy * fy);
	if (fsize > 0.f)
	{
		fx /= fsize;
		fy /= fsize;
		fx *= fAmount;
		fy *= fAmount;
	}

	// float bulForce = 1.f;

	float bullVSize = 1;
	if (bullVSize > 0.f && m_obj[HERO_ID]->CanShootBullet() == true)
	{
		float bulletSpeed = 5.f;

		float heroX, heroY, heroZ;
		float heroVel_X, heroVel_Y, heorVel_Z;

		m_obj[HERO_ID]->getPos(&heroX, &heroY, &heroZ);
		m_obj[HERO_ID]->getVel(&heroVel_X, &heroVel_Y, &heorVel_Z);


		int idx = AddObject(
			heroX, heroY, heroZ,//pos
			0.05, 0.05, 0.05,//size
			-1, 0.f, 0.f,//vel
			1, 1, 1, 1,//rgba
			1.f,//fri
			0.6f,//mass
			TYPE_BULLET, //type
			10.0f, g_bullet);

		m_obj[idx]->AddForce(0.5, 0.5, 0.5, 1.3);//1.3 :폭발시간
		m_obj[idx]->setParentObj(m_obj[HERO_ID]);
		m_obj[HERO_ID]->resetBulletCoolTime();
		
	}


	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_obj[i] != NULL)
		{
			m_obj[i]->Update(eTiemInSecond);
		}
	}
}

void ScnMgr::DoGarbageCollection() {

}

int ScnMgr::AddObject(float x, float y, float z, float sx, float sy, float sz, float vx, float vy, float vz, 
					  float r, float g, float b, float a, float mass, float FricCoef, int type,float hp,int texID){
	int idx = -1;
	for (int i = 0; i < MAX_OBJ_COUNT; ++i) {
		if (m_obj[i] == NULL) {
			idx = i;
			break;
		}
	}

	//07
	//인덱스가 꽉 차있을경우 
	//프로그램을 죽이는 것보다 출력로그를 띄워주는것이 좋다 
	if (idx == -1) {
		std::cout << "no more empty sloat" << std::endl;
		return -1;//addobj는 -1을 리턴한다. 나는 더이상 일을 할수없다고 알려준다. 출력로그와 함께 
	}

	//10.01
	m_obj[idx] = new Object();
	m_obj[idx]->setPos(x, y, z);
	m_obj[idx]->setVol(sx, sy, sz);
	m_obj[idx]->setVel(vx, vy, vz);
	m_obj[idx]->setColor(r, g, b, a);
	m_obj[idx]->setMass(mass);
	m_obj[idx]->SetFricCoef(FricCoef);
	m_obj[idx]->SetType(type);
	m_obj[idx]->SetHP(hp);
	return idx;

}

void ScnMgr::DeleteObject(int idx)
{

}


void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{

	if (key == GLUT_KEY_UP)
	{
		m_keyU = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyD = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyL = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyR = true;
	}

}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_keyU = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyD = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyL = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyR = false;
	}
}

//void ScnMgr::keyDownInput(unsigned char key, int x, int y)
//{
//	if (key == 'w' || key == 'W'){//w일 경우에는 keyW가 트루가 됨
//		m_keyW = true;
//	}
//	else if (key == 'a' || key == 'A'){
//		m_keyA = true;
//	}
//	else if (key == 's' || key == 'S'){
//		m_keyS = true;
//	}
//	else if (key == 'd' || key == 'D'){
//		m_keyD = true;
//	}
//	else if (key == ' ') {
//		m_keySP = true;
//	}
//}

//void ScnMgr::keyUpInput(unsigned char key, int x, int y)
//{
//	if (key == 'w' || key == 'W'){
//		m_keyW = false;
//	}
//	else if (key == 'a' || key == 'A'){
//		m_keyA = false;
//	}
//	else if (key == 's' || key == 'S'){
//		m_keyS = false;
//	}
//	else if (key == 'd' || key == 'D'){
//		m_keyD = false;
//	}
//	else if (key == ' ') {
//		m_keySP = false;
//	}
//}

//void ScnMgr::SpecialKeyDownInput(unsigned char key, int x, int y){
//	if (key == 'w' || key == 'W') {
//		m_keyW = true;
//	}
//	if (key == 'a' || key == 'A') {
//		m_keyA = true;
//	}
//	if (key == 's' || key == 'S') {
//		m_keyS = true;
//	}
//	if (key == 'd' || key == 'D') {
//		m_keyD = true;
//	}
//}

