/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include "ScnMgr.h"

ScnMgr  *g_ScnMgr = NULL;

//10.04
int g_prevTimeInMillisecond = 0;

void RenderScene(int temp)
{
	

	//10.04
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
	int elapsedTime = currentTime - g_prevTimeInMillisecond;
	//elapsed : 이전 update 불린 이후부터 다시 불렸을때까지 지난 시간. 

	g_prevTimeInMillisecond = currentTime;

	std::cout << elapsedTime << std::endl;

	//render calls
	g_ScnMgr->Update(elapsedTime / 1000.0f); //1000으로 나누는것 까먹지 말기 . 
	g_ScnMgr->RenderScene();


	//10.04 
	//오브젝트에 속도 구현하고 
	//오브젝트 안에 멤버함수로서 업데이트라는 함수를 만들어주고 
	//그 인자로 float elapseTIme을 초단위로 넘겨주기
	// 오브젝트는 위치와 속도를 가지고있을거니까 업데이트 할때
	//위치 식을 사용해서 구현하면됨 
	// 메인-> 업데이트 -> 렌더

	glutSwapBuffers();
	glutTimerFunc(16, RenderScene, 16); //타이머에 콜백을 달아주는것임. 
	//glutSwapBuffers();
	////더블버퍼링 구조에서 필수적으로 호출해야되는것.
	////이게 호출이안되면 싱글버퍼만돌아서 화면변화가없음. 
	////스왑버퍼. 더블버퍼링 
	////이건 #include "Dependencies\glew.h"안에 있음. 


}

//아무런 입력이 없을때 호출되는것 
void Idle(void)
{
//	RenderScene();
	//타이머 콜백으로 랜더씬이 지속적으로 호출되게 하려고 
}

void MouseInput(int button, int state, int x, int y)
{
	//RenderScene();
}

void KeyInput(unsigned char key, int x, int y)
{
//	RenderScene();
}

void KeyDownInput(unsigned char key, int x, int y)
{
	//g_ScnMgr->keyDownInput(key, x, y);

}
void KeyUpInput(unsigned char key, int x, int y)
{
	//g_ScnMgr->keyUpInput(key, x, y);
}

void SpecialKeyDownInput(int key, int x, int y)
{
	//RenderScene();
		g_ScnMgr->SpecialKeyDownInput(key, x, y);
}

void SpecialKeyUpInput(int key, int x, int y)
{
	//RenderScene();
		g_ScnMgr->SpecialKeyUpInput(key, x, y);
}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv); 
	
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(500, 700);
	glutCreateWindow("Hani's 1942 GAME");

	
	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	//initialize scnMar
	g_ScnMgr = new ScnMgr;


	glutDisplayFunc(Idle);//glutDisplayFunc(render)에서 변경 
	glutIdleFunc(Idle);
	//glutKeyboardFunc(KeyInput);
	
	glutKeyboardFunc(KeyDownInput);// key down 시 호출되는 이벤트
	glutKeyboardUpFunc(KeyUpInput);// key up 시 호출되는 이벤트
	
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	//10.04
	//glut_init 호출후 지난 시간을 millisecond로 넘겨줌. 
	g_prevTimeInMillisecond = glutGet(GLUT_ELAPSED_TIME);

	glutTimerFunc(16, RenderScene, 16); //16초이후에 함수를 호출하라는 함수 //강제로 호출

	glutMainLoop();

	//delete g_Renderer;

    return 0;
}
