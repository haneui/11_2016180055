#pragma once
#include "Object.h"
#include "Sound.h"
class physics
{
public:
	physics();
	~physics();

	bool ISOverlap(Object *A, Object *B, int method = 0);

	bool isCollide(Object *A, Object * B, int type);
	void processCollision(Object * a, Object * b);
private:
	bool BBOverlapTest(Object *A,Object *B);
	bool BBColloision_test(Object *A, Object *B);
};

