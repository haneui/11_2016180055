#pragma once

class Object{
private:

	float m_r, m_g, m_b, m_a;	//color
	float m_posX, m_posY, m_posZ; //position
	float m_mass; // mass
	float m_velX, m_velY, m_velZ;// velocity
	float m_accX, m_accY, m_accZ;//acceleration
	float m_volX, m_volY, m_volZ; //volume
	float m_fricCoef;
	int m_type;
	float m_hp;
	//float vSize;
	float m_physics;
	int m_texID;

	float m_remainBulletCoolTime = 0.f; //얼마나 시간이 남았는가 
	float m_bulletCoolTime = 0.2f;//시간이 다 지나고 나서 리셋하기 위한 함수 

	Object *m_parent;

public:
	Object();
	~Object();

	void InitThis();

	void setColor(float r, float g, float b,float a);
	void GetColor(float *r, float *g, float *b, float *a);
	
	void setVel(float x, float y, float z);
	void getVel(float *x, float *y, float *z);

	void setACC(float x, float y, float z);
	void getACC(float *x, float *y, float *z);

	void setPos(float x, float y, float z);
	void getPos(float *x, float *y, float *z);

	void setVol(float x, float y, float z);
	void getVol(float *x, float *y, float *z);

	void setMass(float m);
	void getMass(float *m);

	void SetFricCoef(float coef);
	void getFricCoef(float *coef);

	void AddForce(float x, float y, float z, float eTimeInSecond);
	//eTime : 힘이 어느시간동안 주어졌는가 

	void Update(float elapsedTime_in_second);

	void SetType(int type);
	void GetType(int *type);

	void setTexture(int id);
	void getTexture(int *id);

	bool CanShootBullet();
	void resetBulletCoolTime();

	void setParentObj(Object * obj);
	bool isAncestor(Object * obj);

	void GetHP(float *hp);
	void SetHP(float hp);
	void Damage(float damage);

};

