#pragma once
#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include "Dependencies\glew.h"

#include "Renderer.h"
#include "Object.h"
#include "Globals.h"
#include "physics.h"
#include "Sound.h"

class ScnMgr
{
private: 


	Renderer* m_renderer = NULL;
	Sound* m_Sound = NULL;
	//Sound* m_BGM = NULL;
	Object *m_obj[MAX_OBJ_COUNT];
	Object *m_Testobj = NULL;
	physics * m_physics = NULL;

	//12 
	//테스트 캐릭터는 인덱스를 가지고있는 변수임. 
	int m_TestChar[MAX_OBJ_COUNT];

	//bool m_keyW = false;
	//bool m_keyS = false;
	//bool m_keyA = false;
	//bool m_keyD = false;
	//bool m_keySP = false;

	bool m_keyU = false;
	bool m_keyL = false;
	bool m_keyD = false;
	bool m_keyR = false;
	bool m_keySP = false;


public:
	ScnMgr();
	~ScnMgr();
	void RenderScene();
	void Update(float eTiemInSecond); 
	void DoGarbageCollection();

	int AddObject(float x, float y, float z, 
		float sx, float sy, float sz, 
		float r, float g, float b, float a,
		float vx, float vy, float vz,
		float mass, float fricCoef, int type,float hp,int texID);

	void DeleteObject(int idx);

	//void keyDownInput(unsigned char key, int x, int y);
	//void keyUpInput(unsigned char key, int x, int y);

	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);

	//void GarbageCollection();
};

