#include "stdafx.h"
#include "Object.h"
#include"Globals.h"
#include <math.h>
#include <float.h>
#include "Sound.h"

Object::Object()
{
	InitThis();
}

Object::~Object()
{
}

void Object::InitThis()
{
	 m_posX = 0; m_posY = 0; m_posZ = -1.f;	//position
	 m_volX = 0; m_volY = 0; m_volZ = -1.f; //volume
	 m_velX = 0; m_velY = 0; m_velZ = 0;	// velocity
	 m_accX = 0; m_accY = 0; m_accZ = -1.f;;//acceleration
	 m_r = 0; m_g = 0; m_b = 0; m_a = -1.f;	//color
	 m_mass = -1.f; // mass
	 m_fricCoef = 0;
	 m_type = 0;
	 m_hp = 0.f;
}

void Object::setPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;

}

void Object::getPos(float * x, float * y, float * z)
{
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;

}

void Object::setVol(float x, float y, float z)
{
	m_volX = x;
	m_volY = y;
	m_volZ = z;
}

void Object::getVol(float * x, float * y, float * z)
{
	*x = m_volX;
	*y = m_volY;
	*z = m_volZ;
}

void Object::setMass(float m)
{
	m_mass = m;
}

void Object::getMass(float * m)
{
	*m = m_mass;
}

void Object::AddForce(float x, float y, float z, float eTimeInSecond)
{
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;
	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX = m_velX + accX * eTimeInSecond;
	m_velY = m_velY + accY * eTimeInSecond;
	m_velZ = m_velZ + accZ * eTimeInSecond;

		//가속도  = 힘/ 질량
}

void Object::Update(float elapsedTime_in_second){

	/////Apply friction////////
	float nForce = m_mass * GRAVITY;//scalar
	float fForce = m_fricCoef * nForce;//scalar
	float velsize =(m_velX * m_velX + m_velY * m_velY + m_velZ * m_velZ);

	if (velsize > 0.f) {
		float fDirX = -1.f * m_velX / velsize;
		float fDirY = -1.f * m_velX / velsize;

		fDirX = fDirX * fForce;
		fDirY = fDirY * fForce;//z개념은 있으나 방향이 없으므로 설정하지 않음

		float fAccX = fDirX / m_mass;
		float fAccY = fDirY / m_mass;

		float newX = m_velX + fAccX * elapsedTime_in_second;
		float newY = m_velY + fAccY * elapsedTime_in_second;

		if (newX*m_velX < 0.f) {
			m_velX = 0.f;
		}
		else
			m_velX = newX;
		if (newY*m_velY < 0.f) {
			m_velY = 0.f;
		}
		else
			m_velY = newY;

		m_velZ = m_velZ-GRAVITY;

		//진행방향에 정지 마찰력을 적용
	}

	//update Postition

	m_posX = m_posX + m_velX * elapsedTime_in_second;//	+(1 / 2)*m_accX * elapsedTime *elapsedTime;
	m_posY = m_posY + m_velY * elapsedTime_in_second;// +(1 / 2)*m_accY * elapsedTime *elapsedTime;
	m_posZ = m_posZ + m_velZ * elapsedTime_in_second;// +(1 / 2)*m_accZ * elapsedTime *elapsedTime;


	if (m_posZ < FLT_EPSILON) {
		m_posZ = 0.f;
		m_velZ = 0.f;
	}


	////Fire bullsts
	//if (m_obj[HERO_ID]->canshootbullet()) {
	//	float bulletVel = 50.f;
	//	float vBulletX, vBulletY, vBulletZ;
	//	vBulletX = vBulletY = vBulletZ = 0.f;



	//}
}

void Object::setVel(float x, float y, float z){
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}

void Object::getVel(float * x, float * y, float * z){
	*x = m_velX;
	*y = m_velY;
	*z = m_velZ;
}

void Object::setACC(float x, float y, float z){
	m_accX = x;
	m_accY = y;
	m_accZ = z;
}

void Object::getACC(float * x, float * y, float * z){
	*x = m_accX;
	*y = m_accY;
	*z = m_accZ;
}

void Object::setColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a; 
}

void Object::GetColor(float * r, float * g, float * b, float * a){
	*r = m_r;
	*g = m_g;
	*b = m_b; 
	*a = m_a;
	
}

void Object::SetFricCoef(float coef) {

	m_fricCoef = coef;
}

void Object::getFricCoef(float *coef) {

	*coef = m_fricCoef;
}

void Object::SetType(int type) {

	m_type = type;
}

void Object::GetType(int *type) {

	*type = m_type;

}
void Object::setTexture(int id)
{
	m_texID = id;
}

void Object::getTexture(int * id)
{
	*id = m_texID;
}

bool Object::CanShootBullet() {
	if (m_remainBulletCoolTime < FLT_EPSILON){
		return	true;
	}
	else {
		return false;
	}
}

void Object::resetBulletCoolTime()
{
	m_remainBulletCoolTime = m_bulletCoolTime;
}

void Object::setParentObj(Object * obj)
{
	m_parent = obj;
}

bool Object::isAncestor(Object * obj)
{
	if (obj != NULL)
	{
		if (obj == m_parent)
		{
			return true;
		}
	}
	return false;
}

void Object::GetHP(float *hp) {
	*hp = m_hp;
}

void Object::SetHP(float hp) {
	m_hp = hp;
}

void Object::Damage(float damage) {

	m_hp = m_hp - damage;
}